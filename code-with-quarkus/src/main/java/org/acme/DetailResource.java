package org.acme;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.acme.models.Detail;
import org.acme.models.Product;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;

@Path("details")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailResource extends PanacheEntityBase {
    @GET
    public List<Detail> getDetail() {
        return Detail.listAll(Sort.ascending("id"));
    }

    @GET
    @Path("{merk}")
    public ResponseBuilder getDetailByid(@PathParam("merk") String merk) {

        Detail detail = Detail.find("merk", merk.toLowerCase()).firstResult();
        if (detail == null) {
            return Response.ok(detail, ">>" + merk + " not found");
        }
        return Response.ok(detail, "detail merk " + merk + " tersedia");

    }

    @POST
    @Path("{id}")
    @Transactional
    public ResponseBuilder InsertDetail(@PathParam("id") Long id, @Valid Detail details) {
        Product product = Product.findById(id);
        if (product == null) {
            return Response.ok(product, "id " + id + " tidak ditemukan");
        }
        details.product = product;
        details.persist();
        Detail.listAll();
        return Response.ok(details, "insert detail ke produk id " + id + " berhasil");
    }

    @PUT
    @Path("updateDetails/{id}")
    @Transactional
    public ResponseBuilder updateDetail(@PathParam("id") Long id, @Valid Detail newDetail) {
        Detail oldDetail = Detail.findById(id);
        if (oldDetail == null) {
            return Response.ok(oldDetail, "id " + id + " tidak ditemukan!");
        }
        oldDetail.garansi = newDetail.garansi;
        oldDetail.masaGaransi = newDetail.masaGaransi;
        oldDetail.stok = newDetail.stok;
        oldDetail.deskripsi = newDetail.deskripsi;
        oldDetail.penilaian = newDetail.penilaian;
        return Response.ok(oldDetail, "update detail dengan id " + id + " berhasil");
    }

    @DELETE
    @Path("delete/{id}")
    @Transactional
    public ResponseBuilder deleteDetailById(@PathParam("id") Long Id) {
        boolean detail = Detail.deleteById(Id);
        if (detail == false) {
            return Response.ok(detail, "detail dengan id " + Id + " tidak ditemukan!");
        }
        return Response.ok(detail, "data dengan id " + Id + " dihapus");
    }

    @DELETE
    @Path("deleteAll")
    @Transactional
    public Long deleteAllDetail() {
        return Detail.deleteAll();
    }

}
