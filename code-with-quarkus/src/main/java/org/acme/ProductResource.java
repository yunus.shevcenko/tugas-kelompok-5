package org.acme;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.acme.models.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;

@Path("products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource extends PanacheEntityBase {
    // method untuk mendapatkan keseluruhan data
    @GET
    public List<Product> getProduct() {
        return Product.listAll(Sort.ascending("id"));
    }

    // method untuk mendapatkan data dengan atribut tertentu
    // method untuk menambahkan data entitas baru
    @POST
    @Transactional
    @Path("insertProduct")
    public ResponseBuilder insertProduct(@Valid Product products) {
        products.persist();
        return Response.ok(products, "product berhasil ditambahkan");

    }

    // method untuk mengubah data entitas yang ada
    @PUT
    @Transactional
    @Path("updateProduct/{id}")
    public ResponseBuilder updateproduct(@PathParam("id") Long id, @Valid Product newProduct) {
        Product oldProduct = Product.findById(id);
        if (oldProduct == null) {
            return Response.ok(oldProduct, "id " + id + " tidak ditemukan!");
        }
        oldProduct.nameProduct = newProduct.nameProduct;
        oldProduct.merk = newProduct.merk;
        oldProduct.price = newProduct.price;
        return Response.ok(oldProduct, "produk dengan id " + id + " berhasil update");
    }

    // method untuk menghapus data dengan Id tertentu
    @DELETE
    @Path("delete/{id}")
    @Transactional
    public ResponseBuilder deleteProductById(@PathParam("id") Long Id) {
        boolean product = Product.deleteById(Id);
        if (product == false) {
            return Response.ok(product, "product dengan id " + Id + " tidak ditemukan!");
        }
        return Response.ok(product, "produk dengan id " + Id + " dihapus");
    }

    /* method untuk menghapus seluruh data yang ada */
    @DELETE
    @Path("deleteAll")
    @Transactional
    public ResponseBuilder deleteAllproduk() {
        Long product = Product.deleteAll();
        if (product == 0) {
            return Response.ok(product, "tidak ada product!");
        }
        return Response.ok(product, "semua product terhapus");
    }

}