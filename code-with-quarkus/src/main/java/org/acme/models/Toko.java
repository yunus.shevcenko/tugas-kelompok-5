package org.acme.models;

import javax.persistence.*;
import javax.persistence.OneToMany;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "toko")
public class Toko extends PanacheEntityBase {
    // attributes go here
    @Id
    @SequenceGenerator(name = "tokoSequence", sequenceName = "toko_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tokoSequence")
    public Long id;

    @NotBlank(message = "masukkan nama toko")
    @Column(name = "name_toko")
    public String nameToko;

    @NotBlank(message = "apakah toko adalah super toko")
    public String alamat;
    
    @NotNull(message = "berapa nomor telepon anda")
    public Integer nomorTel;

    @OneToMany(mappedBy = "toko")
    public List<Product> product;

    //Getter and setter go here
    @JsonGetter
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
