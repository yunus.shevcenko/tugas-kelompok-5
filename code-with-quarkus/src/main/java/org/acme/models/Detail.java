package org.acme.models;

import java.sql.Date;

import javax.persistence.*;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.*;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "details")
public class Detail extends PanacheEntityBase {
    @Id
    @SequenceGenerator(name = "detailSequence", sequenceName = "detail_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "detailSequence")
    // membuat atribut
    public Long id;
    @NotBlank(message = "masukkan jenis garansi produk anda")
    public String garansi;
    @NotNull(message = "tentukan masa garansi produk anda")
    public Date masaGaransi;
    @NotNull(message = "cantumkan produk")
    public Integer stok;
    @NotBlank(message = "deskripsikan produk!")
    public String deskripsi;
    @Max(value = 5)
    @NotNull(message = "berikan penilaian pada produk")
    public Integer penilaian;

    // membuat relasi dan foregin key
    @OneToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public Product product;

    @JsonIgnore
    public Product getProduct() {
        return product;
    }

    @JsonSetter
    public void setProduct(Product product) {
        this.product = product;
    }

    @JsonGetter
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
